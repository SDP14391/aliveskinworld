-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 06, 2015 at 05:30 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `alive`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) NOT NULL,
  `mname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `pincode` int(11) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `e_code` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `insdate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fname`, `mname`, `lname`, `address`, `pincode`, `city`, `state`, `mobile`, `email`, `e_code`, `username`, `password`, `user_type`, `area`, `insdate`) VALUES
(1, '', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:14:39'),
(2, '', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:15:00'),
(3, '', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:15:05'),
(4, '', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:15:56'),
(5, '', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:16:02'),
(6, '', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:16:09'),
(7, '', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:16:40'),
(8, 'anokhi', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:20:04'),
(9, 'anokhi', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:23:21'),
(10, 'anokhi', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:26:17'),
(11, 'anokhi', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:26:58'),
(12, 'anokhi', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:27:02'),
(13, 'anokhi', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:27:35'),
(14, 'anokhi', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:27:38'),
(15, 'anokhi', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:28:35'),
(16, 'anokhi', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:28:38'),
(17, 'anokhi', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:29:54'),
(18, 'anokhi', 'k', 'patel', 'gota', 382481, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '', 'hello', '2015-07-04 00:29:57'),
(19, 'aaa', 'aaa', 'aaz', 'aaaa', 1213, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '- Select Products', 'hello', '2015-07-04 00:35:19'),
(20, 'aaa', 'aaa`', 'aaa`', 'aaa`', 123, '13', '1', '9904450799', 'kiran_patel80@yahoo.com', '', 'kiran', 'kiran', '2', 'hello', '2015-07-04 00:36:51');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
