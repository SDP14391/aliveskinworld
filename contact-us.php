<?php include 'admin/db.php'; ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Alive | Contact Us</title>
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/stylesheet.css"   rel="stylesheet" />
<link href="fonts/stylesheet.css"   rel="stylesheet" />

<link rel="stylesheet" href="css/bootstrap.min.css">
 <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
 
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
 
</head>

<body>
<div class="container">

<!-- header start -->
 <?php include 'header.php'; ?>

   <!-- header end -->
</div>
 <div class="section1 ">
<div class="container">
<div class="col-md-6">
  <!-- responsive slider -->
 <h1 class="big-title" >Factory</h1>    
        
          <address>
  <strong><h2> <span>Alive   </span> </h2></strong> 
  819/D, Rakanpur P.O. Santej, <br>
  Ta: Kalol,District: Gandhinagar, <br>
  Pin: 382721, Gujarat. <br />
  <abbr>Phone or FAX :</abbr> (02764) 286008 <br />
 <strong> Mobile : </strong> +91 98981 59545 , 98250 48631
  <br />
  <strong>Email: </strong>info@aliveskinworld.com , marketing@aliveskinworld.com<br />
 <strong> Website : </strong> <a href="http://www.aliveskinworld.com" target="_blank">www.aliveskinworld.com </a>
</address>
 </div>
       <div class="col-md-6">
        <h1 class="big-title" >Office</h1>   
        
          <address>
  <strong><h2> <span>Alive Lifescience Pvt. Ltd. </span> </h2></strong> 
  103 A , B Block , Dev aditya Arcade,<br />
  Opp. Baghbhan party plot , <br />
  Thaltej Shilaj Road , Thaltej,<br />
  Ahmedabad - 380054 <br />
   <strong>Email: </strong>info@aliveskinworld.com , marketing@aliveskinworld.com <br />
 <strong> Website : </strong> <a href="http://www.aliveskinworld.com" target="_blank">www.aliveskinworld.com </a>
</address> 
	</div>
         </div>
</div></div>

 <?php include 'footer.php'; ?>

    <script src="js/app.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

  </body>
</html>
